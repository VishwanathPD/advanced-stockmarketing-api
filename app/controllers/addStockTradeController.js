const mongoose = require('mongoose');
const shortid = require('shortid');

const response = require('../libs/responseLib')
const logger = require('../libs/loggerLib');

const check = require('../libs/checkLib');
const stockPortfolioM =  require('../models/stockPortfolio');
const stockPortfolioModel = mongoose.model('stockPortfolioModel');
const stockTransaction = require('../service/stockTransactionService');
const stockPortfolio = require('../service/stockPortfolioService');

//----------------------------ADD TRADE -----------------------------------//

let addTrade = (req, res) =>{
    
    let validateTrade = () =>{
        return new Promise((resolve, reject)=>{
            if(check.isEmpty(req.body.tickerSymbol) || check.isEmpty(req.body.tradePrice) || check.isEmpty(req.body.noOfShares) || check.isEmpty(req.body.tradeType)) {
                let apiResponse = response.generate(true, 'Parameters are missing. Please check', 406, null);
                // res.send(apiResponse);
                reject(apiResponse);
            }    
            //check if tradeType is only buy or sell
            if(!(req.body.tradeType === "buy" ||  req.body.tradeType === "sell" )){
                let apiResponse = response.generate(true, 'Trade type can be only sell or buy', 406, null);
                // res.send(apiResponse);
                reject(apiResponse);
            }
            //tickerSymbol should be string
            if(typeof req.body.tickerSymbol !== "string"){
                let apiResponse = response.generate(true, 'tickerSymbol should be string', 406, null);
                // res.send(apiResponse);
                reject(apiResponse);
            }
            //check if noOfShares is number
            if(typeof req.body.noOfShares !== "number"){
                let apiResponse = response.generate(true, 'noOfShares should be number and greater than 0', 406, null);
                // res.send(apiResponse);
                reject(apiResponse);
            }
            //check if tradePrice is number
            if(typeof req.body.tradePrice !== "number"){
                let apiResponse = response.generate(true, 'tradePrice should be number and greater than 0', 406, null);
                // res.send(apiResponse);
                reject(apiResponse);
            }
            //check if noOfShares > 0
            if(req.body.noOfShares <= 0 || req.body.tradePrice > Number.MAX_SAFE_INTEGER){
                let apiResponse = response.generate(true, 'No of shares should be greater than 0 and less than  2^53', 406, null);
                // res.send(apiResponse);
                reject(apiResponse);
            }
             //check if tradePrice > 0
             if(req.body.tradePrice <= 0 || req.body.tradePrice > Number.MAX_SAFE_INTEGER){
                let apiResponse = response.generate(true, 'Trade price should be greater than 0 and less than  2^53', 406, null);
                // res.send(apiResponse);
                reject(apiResponse);
            }
            else{
                resolve(req)
            }    
        })
    }//end of validateTrade

    let saveTrade = () =>{
        return new Promise( (resolve, reject)=>{
            let newTradeDetails={
                newSymbol: req.body.tickerSymbol,
                newTradePrice: req.body.tradePrice,
                newShares: req.body.noOfShares,
                newTradeType: req.body.tradeType
            }
            //search tickerSymbol in Portfolio collection
            stockPortfolioModel.find({tickerSymbol:newTradeDetails.newSymbol})
            .lean()
            .exec((err,result)=>{
                if(err){
                    logger.error(err.message, 'addStockTradeController: getTradeByName', 10)
                    let apiResponse = response.generate(true, 'Failed To Find stock in portfolio', 500, null)
                    res.send(apiResponse)   
                }else if (check.isEmpty(result)){
                    // Stock does not exist in portfolio collection
                    if(newTradeDetails.newTradeType === "buy"){
                        //add to transaction and portfolio collection
                         stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true").then((resolve)=>{
                            stockPortfolio.addToPortfolio(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, resolve.transactionId).then((innerResolve)=>{
                              const resp = { 
                                  tradeId : resolve.tradeId 
                              };
                              const apiResponse = response.generate(false, 'Trade executed successfully', 200, resp)
                              res.send(apiResponse)
                            }).catch((err)=>{
                                logger.error(err.message, 'addStockTradeController: saveTrade', 10);
                                const apiResponse = response.generate(true, 'Failed To execute trade', 500, null);
                                res.send(apiResponse)
                            })
                        }).catch((err)=>{
                            logger.error(err.message, 'addStockTradeController: saveTrade', 10);
                            const apiResponse = response.generate(true, 'Failed To execute trade', 500, null);
                            res.send(apiResponse)
                        })
                    }
                    else if(newTradeDetails.newTradeType === "sell"){
                        //error! -> you dont have enough stocks to sell!
                        let apiResponse = response.generate(true, 'You do not have enough stocks to sell', 500, null)
                        res.send(apiResponse)
                    }
                }else{
                    //Stock exists in portfolio collection
                    //get details of this symbol from portfolio collection
                    const portfolioDetails=result;
                    if(newTradeDetails.newTradeType === "buy"){
                        //calculate shares and averageBuyPrice
                        const newPortfolioShares = parseInt(portfolioDetails[0].noOfShares) + parseInt(newTradeDetails.newShares);
                        const newPortfolioAvBuyPrice = ((portfolioDetails[0].noOfShares*portfolioDetails[0].averageBuyPrice) + (newTradeDetails.newShares* newTradeDetails.newTradePrice))/(newPortfolioShares)
                        //add to transaction collection and portfolio collection
                        stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true").then((resolve)=>{
                            stockPortfolio.updatePortfolioBySymbol(newTradeDetails.newSymbol, newPortfolioAvBuyPrice, newPortfolioShares, resolve.transactionId).then((innerResolve)=>{
                              logger.info(innerResolve, 'addStockTradeController: saveTrade', 10);
                              const resp = { 
                                  tradeId : resolve.tradeId 
                              };
                              const apiResponse = response.generate(false, 'Trade executed successfully', 200, resp)
                              res.send(apiResponse)
                            }).catch((err)=>{
                                logger.error(err.message, 'addStockTradeController: saveTrade', 10);
                                const apiResponse = response.generate(true, 'Failed To execute trade', 500, null);
                                res.send(apiResponse)
                            })
                        }).catch((err)=>{
                            logger.error(err.message, 'addStockTradeController: saveTrade', 10);
                            const apiResponse = response.generate(true, 'Failed To execute trade', 500, null);
                            res.send(apiResponse)
                        })
                    }
                    else if(newTradeDetails.newTradeType === "sell"){
                        //calculate shares left 
                        const newPortfolioShares = parseInt(portfolioDetails[0].noOfShares) - parseInt(newTradeDetails.newShares);
                        if(newPortfolioShares >= 0){
                            if(newPortfolioShares === 0){
                                //delete the stock from portfolio
                                stockPortfolio.deleteTickerSymbolPortfolio(portfolioDetails[0].tickerSymbol);
                                //add to transsaction collection
                                stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true").then((resolve)=>{
                                    const resp = { 
                                        tradeId : resolve.tradeId 
                                    };
                                    const apiResponse = response.generate(false, 'Trade executed successfully', 200, resp)
                                    res.send(apiResponse)
                                }).catch((err)=>{
                                    logger.error(err.message, 'addStockTradeController: saveTrade', 10);
                                    const apiResponse = response.generate(true, 'Failed To execute trade', 500, null);
                                    res.send(apiResponse)
                                })
                            }
                            else{
                                //update in transaction and portfolio collection
                                stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true").then((resolve)=>{
                                    stockPortfolio.updatePortfolioBySymbol(newTradeDetails.newSymbol, portfolioDetails[0].averageBuyPrice, newPortfolioShares, resolve.transactionId).then((innerResolve)=>{
                                      logger.info(innerResolve, 'addStockTradeController: saveTrade', 10);
                                      const resp = { 
                                          tradeId : resolve.tradeId 
                                      };
                                      const apiResponse = response.generate(false, 'Trade executed successfully', 200, resp)
                                      res.send(apiResponse)
                                    }).catch((err)=>{
                                        const apiResponse = response.generate(true, 'Failed To execute trade', 500, null);
                                        res.send(apiResponse)
                                    })
                                }).catch((err)=>{
                                    const apiResponse = response.generate(true, 'Failed To execute trade', 500, null);
                                    res.send(apiResponse)
                                })                           
                            }
                        }
                        else{
                            //error! -> you dont have enough stocks to sell!
                            let apiResponse = response.generate(true, 'You do not have enough stocks to sell', 500, null)
                            res.send(apiResponse)
                        }
                    }
                }
            })
        })
    } 
    validateTrade(req, res)
    .then(saveTrade)
    .then((resolve)=>{
        let apiResponse = response.generate(false, 'Trade executed successfully', 200, resolve)
        res.send(apiResponse)
    })
    .catch((err)=>{
        logger.error(err.message, 'addStockTradeController: AddTrade', 10);
        let apiResponse = response.generate(true, 'Failed To Save Trade', 500, null);
        res.send(err)
    })

}//end of add trade

//---------------------------------END OF ADD TRADE -------------------------------------//

module.exports = {
    addTrade,
}