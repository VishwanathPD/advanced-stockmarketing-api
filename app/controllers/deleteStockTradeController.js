const mongoose = require('mongoose');
const shortid = require('shortid');

const response = require('../libs/responseLib')
const logger = require('../libs/loggerLib');

const check = require('../libs/checkLib');
const stockPortfolioM =  require('../models/stockPortfolio');
const stockTransactionM = require('../models/stockTransaction');
const stockTransactionModel = mongoose.model('stockTransactionModel');
const stockPortfolioModel = mongoose.model('stockPortfolioModel');
const stockTransaction = require('../service/stockTransactionService');
const stockPortfolio = require('../service/stockPortfolioService');

//----------------------------UPDATE TRADE -----------------------------------//

let deleteTransaction = (req, res) =>{
    
    let deleteTrade = () =>{
        return new Promise( (resolve, reject)=>{
            let deleteTradeDetails={
                newTradeId: req.body.tradeId
            }
            //find if the trade id exists and it is recent transaction in transaction collection
            stockTransactionModel.find({
                $and: [
                    { tradeId : deleteTradeDetails.newTradeId },
                    { recent : true }
                ]
            })
            .lean()
            .exec((err,result)=>{
                if(err){
                    logger.error(err.message, 'deleteStockTradeController: findTrade in transaction', 10)
                    let apiResponse = response.generate(true, 'Specified Trade Id doesnot exist', 404, null)
                    res.send(apiResponse)   
                }else if (check.isEmpty(result)){
                    let apiResponse = response.generate(true, 'No Trade details Found', 404, null)
                    res.send(apiResponse)
                }else{
                    logger.info(deleteTradeDetails, 'deleteStockTradeController: findTrade in transaction', 10)
                    const existingTransaction = result;
                    //check if previous transacted stock is in portfolio collection
                    stockPortfolioModel.find({ tickerSymbol : existingTransaction[0].tickerSymbol })
                    .lean()
                    .exec((err,result)=>{
                        if(err){
                            logger.error(err.message, 'deleteStockTradeController: findSymbol in portfolio', 10)
                            let apiResponse = response.generate(true, 'Specified Trade Id doesnot exist', 404, null)
                            res.send(apiResponse)   
                        }else if (check.isEmpty(result)){
                            stockTransaction.addToTransaction(existingTransaction[0].tickerSymbol, existingTransaction[0].tradePrice, existingTransaction[0].noOfShares, existingTransaction[0].tradeType, recent="true").then((resolve)=>{
                                stockPortfolio.addToPortfolio(existingTransaction[0].tickerSymbol, existingTransaction[0].tradePrice, existingTransaction[0].noOfShares, resolve.transactionId).then((innerResolve)=>{
                                  const resp = { 
                                      tradeId : resolve.tradeId 
                                  };
                                  const apiResponse = response.generate(false, 'Trade executed successfully', 200, resp)
                                  res.send(apiResponse)
                                }).catch((err)=>{
                                    logger.error(err.message, 'deleteStockTradeController: saveTrade', 10);
                                    const apiResponse = response.generate(true, 'Failed To execute trade', 500, null);
                                    res.send(apiResponse)
                                })
                            }).catch((err)=>{
                                logger.error(err.message, 'deleteStockTradeController: saveTrade', 10);
                                const apiResponse = response.generate(true, 'Failed To execute trade', 500, null);
                                res.send(apiResponse)
                            })
                        }else{
                            const portfolioDetails = result;
                            //Delete logic starts
                            if(existingTransaction[0].tradeType==="buy"){
                                const newPortfolioShares = parseInt(portfolioDetails[0].noOfShares) - parseInt(existingTransaction[0].noOfShares);
                                if(newPortfolioShares>=0){
                                    if(newPortfolioShares === 0){
                                        //delete the stock from portfolio
                                        stockPortfolio.deleteTickerSymbolPortfolio(existingTransaction[0].tickerSymbol);
                                    }
                                    else{
                                        const newPortfolioAvBuyPrice = ((portfolioDetails[0].noOfShares*portfolioDetails[0].averageBuyPrice) - (existingTransaction[0].noOfShares*existingTransaction[0].tradePrice))/(newPortfolioShares);
                                        //add to transaction collection and update portfolio collection
                                        logger.info(newPortfolioAvBuyPrice+" "+newPortfolioShares, 'deleteStockTradeController: deleteTrade', 10);
                                        stockTransaction.addToTransaction(existingTransaction[0].tickerSymbol, existingTransaction[0].tradePrice, existingTransaction[0].noOfShares, "delete", recent="true", existingTransaction[0].tradeId ).then((resolve)=>{
                                            stockPortfolio.updatePortfolioBySymbol(existingTransaction[0].tickerSymbol, newPortfolioAvBuyPrice, newPortfolioShares, resolve.transactionId).then((innerResolve)=>{
                                            logger.info(innerResolve, 'deleteStockTradeController: deleteTrade', 10);
                                            const resp = { 
                                                tradeId : resolve.tradeId 
                                            };
                                            const apiResponse = response.generate(false, 'Trade deleted successfully', 200, resp)
                                            res.send(apiResponse)
                                            }).catch((err)=>{
                                                logger.error(err.message, 'deleteStockTradeController: deleteTrade', 10);
                                                const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                res.send(apiResponse)
                                            })
                                        }).catch((err)=>{
                                            logger.error(err.message, 'deleteStockTradeController: deleteTrade', 10);
                                            const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                            res.send(apiResponse)
                                        })
                                    }
                                }
                                
                            }
                            else if(existingTransaction[0].tradeType==="sell"){
                                const newPortfolioShares = parseInt(portfolioDetails[0].noOfShares) + parseInt(existingTransaction[0].noOfShares);
                                if(newPortfolioShares>=0){
                                    if(newPortfolioShares === 0){
                                        //delete the stock from portfolio
                                        stockPortfolio.deleteTickerSymbolPortfolio(existingTransaction[0].tickerSymbol);
                                    }
                                    else{
                                        //av buy price remains same
                                        //add to transaction collection and update portfolio collection
                                        stockTransaction.addToTransaction(existingTransaction[0].tickerSymbol, existingTransaction[0].tradePrice, existingTransaction[0].noOfShares, "delete", recent="true", existingTransaction[0].tradeId ).then((resolve)=>{
                                            stockPortfolio.updatePortfolioBySymbol(existingTransaction[0].tickerSymbol, portfolioDetails[0].averageBuyPrice, newPortfolioShares, resolve.transactionId).then((innerResolve)=>{
                                            logger.info(innerResolve, 'deleteStockTradeController: deleteTrade', 10);
                                            const resp = { 
                                                tradeId : resolve.tradeId 
                                            };
                                            const apiResponse = response.generate(false, 'Trade deleted successfully', 200, resp)
                                            res.send(apiResponse)
                                            }).catch((err)=>{
                                                logger.error(err.message, 'deleteStockTradeController: deleteTrade', 10);
                                                const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                res.send(apiResponse)
                                            })
                                        }).catch((err)=>{
                                            logger.error(err.message, 'deleteStockTradeController: deleteTrade', 10);
                                            const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                            res.send(apiResponse)
                                        })
                                    }
                                }
                            }
                        }
                    })
                }
            })
        })
    } 
    deleteTrade(req, res)
    .then((resolve)=>{
        let apiResponse = response.generate(false, 'Trade deleted successfully', 200, resolve)
        res.send(apiResponse)
    })
    .catch((err)=>{
        logger.error(err.message, 'deleteStockTradeController: AddTrade', 10);
        let apiResponse = response.generate(true, 'Failed To Delete Trade', 500, null);
        res.send(apiResponse)
    })

}//end of delete trade

//---------------------------------END OF DELETE TRADE -------------------------------------//

module.exports = {
    deleteTransaction,
}