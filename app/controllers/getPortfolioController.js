const mongoose = require('mongoose');
const shortid = require('shortid');

const response = require('../libs/responseLib')
const logger = require('../libs/loggerLib');

const check = require('../libs/checkLib');
const stockPortfolioM =  require('../models/stockPortfolio');
const stockPortfolioModel = mongoose.model('stockPortfolioModel');

//----------------------------GET TRANSACTIONS -----------------------------------//
let getPortfolio=(req, res)=>{
    stockPortfolioModel.find()
    .select('-__v -_id -portfolioId -transactionId')   //remove additional fields
    .lean()
    .exec( (err, result)=>{
        if(err){
            logger.error(err.message, 'Get Transaction Controller: getAllTransactions', 10)
            let apiResponse = response.generate(true, 'Failed To Find Transaction', 500, null)
            res.send(apiResponse)   
        }else if (check.isEmpty(result)){
            let apiResponse = response.generate(true, 'No Transaction Found', 404, null)
            res.send(apiResponse)
        }else{
            let apiResponse = response.generate(false, 'Portfolio Found', 200, result)
            res.send(apiResponse)
        }
    })

}
//---------------------------------END OF GET TRANSACTIONS -------------------------------------//

module.exports = {
    getPortfolio,
}