const mongoose = require('mongoose');
const shortid = require('shortid');

const response = require('../libs/responseLib')
const logger = require('../libs/loggerLib');

const check = require('../libs/checkLib');
const stockPortfolioM = require('../models/stockPortfolio');
const stockPortfolioModel = mongoose.model('stockPortfolioModel');

//----------------------------GET TRANSACTIONS -----------------------------------//
let getReturns = (req, res) => {
    stockPortfolioModel.find()
        .select('-__v -_id -portfolioId -transactionId')     //remove additional fields
        .lean()
        .exec((err, result) => {
            if (err) {
                logger.error(err.message, 'Get Returns Controller: getReturns', 10)
                let apiResponse = response.generate(true, 'Failed To Find Transactions', 500, null)
                res.send(apiResponse)
            } else if (check.isEmpty(result)) {
                let apiResponse = response.generate(true, 'No Transactions Found', 404, null)
                res.send(apiResponse)
            } else {
                // logger.info(result, 'Get Returns Controller: getAllTransactions', 10)
                let returns = 0;
                const currentPrice = 100;
                result.forEach((stock) => {
                    returns = returns + (currentPrice - stock.averageBuyPrice) * stock.noOfShares;
                });
                let apiResponse = response.generate(false, 'Portfolio Found', 200, { "Total returns": returns })
                res.send(apiResponse)
            }
        })
}
//---------------------------------END OF GET TRANSACTIONS -------------------------------------//

module.exports = {
    getReturns,
}