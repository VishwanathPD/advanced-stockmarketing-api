const mongoose = require('mongoose');
const shortid = require('shortid');

const response = require('../libs/responseLib')
const logger = require('../libs/loggerLib');

const check = require('../libs/checkLib');
const stockTransactionM = require('../models/stockTransaction');
const stockTransactionModel = mongoose.model('stockTransactionModel');

//----------------------------GET TRANSACTIONS -----------------------------------//
let getTransaction=(req, res)=>{
    stockTransactionModel.find()
    .select('-__v -_id -recent')   //remove additional fields
    .lean()
    .exec( (err, result)=>{
        if(err){
            logger.error(err.message, 'Get Portfolio Controller: getTransaction', 10)
            let apiResponse = response.generate(true, 'Failed To Find Transaction', 500, null)
            res.send(apiResponse)   
        }else if (check.isEmpty(result)){
            let apiResponse = response.generate(true, 'No Transactions Found', 404, null)
            res.send(apiResponse)
        }else{
            let apiResponse = response.generate(false, 'Transaction Details Found', 200, result)
            res.send(apiResponse)
        }
    })

}
//---------------------------------END OF GET TRANSACTIONS -------------------------------------//

module.exports = {
    getTransaction,
}