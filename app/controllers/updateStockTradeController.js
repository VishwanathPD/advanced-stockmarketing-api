const mongoose = require('mongoose');
const shortid = require('shortid');

const response = require('../libs/responseLib')
const logger = require('../libs/loggerLib');

const check = require('../libs/checkLib');
const stockPortfolioM =  require('../models/stockPortfolio');
const stockTransactionM = require('../models/stockTransaction');
const stockTransactionModel = mongoose.model('stockTransactionModel');
const stockPortfolioModel = mongoose.model('stockPortfolioModel');
const stockTransaction = require('../service/stockTransactionService');
const stockPortfolio = require('../service/stockPortfolioService');

//----------------------------UPDATE TRADE -----------------------------------//

let editTransaction = (req, res) =>{
    
    let validateTrade = () =>{
        return new Promise((resolve, reject)=>{
            if(check.isEmpty(req.body.tickerSymbol) || check.isEmpty(req.body.tradePrice) || check.isEmpty(req.body.noOfShares) || check.isEmpty(req.body.tradeType)) {
                let apiResponse = response.generate(true, 'Parameters are missing. Please check', 406, null);
                // res.send(apiResponse);
                reject(apiResponse);
            }    
            //check if tradeType is only buy or sell
            if(!(req.body.tradeType === "buy" ||  req.body.tradeType === "sell" )){
                let apiResponse = response.generate(true, 'Trade type can be only sell or buy', 406, null);
                // res.send(apiResponse);
                reject(apiResponse);
            }
            //tickerSymbol should be string
            if(typeof req.body.tickerSymbol !== "string"){
                let apiResponse = response.generate(true, 'tickerSymbol should be string', 406, null);
                // res.send(apiResponse);
                reject(apiResponse);
            }
            //check if noOfShares is number
            if(typeof req.body.noOfShares !== "number"){
                let apiResponse = response.generate(true, 'noOfShares should be number and greater than 0', 406, null);
                // res.send(apiResponse);
                reject(apiResponse);
            }
            //check if tradePrice is number
            if(typeof req.body.tradePrice !== "number"){
                let apiResponse = response.generate(true, 'tradePrice should be number and greater than 0', 406, null);
                // res.send(apiResponse);
                reject(apiResponse);
            }
            //check if noOfShares > 0
            if(req.body.noOfShares <= 0 || req.body.tradePrice > Number.MAX_SAFE_INTEGER){
                let apiResponse = response.generate(true, 'No of shares should be greater than 0 and less than  2^53', 406, null);
                // res.send(apiResponse);
                reject(apiResponse);
            }
             //check if tradePrice > 0
             if(req.body.tradePrice <= 0 || req.body.tradePrice > Number.MAX_SAFE_INTEGER){
                let apiResponse = response.generate(true, 'Trade price should be greater than 0 and less than  2^53', 406, null);
                // res.send(apiResponse);
                reject(apiResponse);
            }
            else{
                resolve(req)
            }    
        })
    }//end of validateTrade

    let updateTrade = () =>{
        return new Promise( (resolve, reject)=>{
            let newTradeDetails={
                newSymbol: req.body.tickerSymbol,
                newTradePrice: req.body.tradePrice,
                newShares: req.body.noOfShares,
                newTradeType: req.body.tradeType,
                newTradeId: req.body.tradeId
            }
            //find if the trade id exists and it is recent transaction in transaction collection
            stockTransactionModel.find({
                $and: [
                    { tradeId : newTradeDetails.newTradeId },
                    { recent : true }
                ]
            })
            .lean()
            .exec((err,result)=>{
                if(err){
                    logger.error(err.message, 'updateStockTradeController: findTrade in transaction', 10)
                    let apiResponse = response.generate(true, 'Specified Trade Id doesnot exist', 500, null)
                    res.send(apiResponse)   
                }else if (check.isEmpty(result)){
                    let apiResponse = response.generate(true, 'No Trade details Found', 404, null)
                    res.send(apiResponse)
                }else{
                    logger.info(newTradeDetails, 'updateStockTradeController: findTrade in transaction', 10)
                    let apiResponse = response.generate(false, 'Trade Details Found', 200, result)
                    const existingTransaction = result;
                    stockPortfolioModel.find({ tickerSymbol : newTradeDetails.newSymbol })
                    .lean()
                    .exec((err,result)=>{
                        if(err){
                            logger.error(err.message, 'updateStockTradeController: findSymbol in portfolio', 10)
                            let apiResponse = response.generate(true, 'Specified Trade Id doesnot exist', 500, null)
                            res.send(apiResponse)   
                        }else if (check.isEmpty(result)){
                            if(newTradeDetails.newTradeType === "sell"){
                                logger.error("err.message", 'updateStockTradeController: findSymbol in portfolio no enough stocks', 10)
                                let apiResponse = response.generate(true, 'You dont have enough stocks to sell', 500, null)
                                res.send(apiResponse) 
                            }
                            else{
                                stockPortfolioModel.find({ tickerSymbol : existingTransaction[0].tickerSymbol}) //dname
                                .lean()
                                .exec((err,result)=>{
                                    if(err){
                                        logger.error(err.message, 'updateStockTradeController: findSymbol in portfolio', 10)
                                        let apiResponse = response.generate(true, 'Specified Trade Id doesnot exist', 500, null)
                                        res.send(apiResponse)   
                                    }else if (check.isEmpty(result)){
                                        //Previously transacted stock details not found in prtfolio collection
                                        //add to transaction collection and portfolio collection
                                        logger.error("err.message", 'updateStockTradeController: findSymbol in portfolio', 10)
                                        let apiResponse = response.generate(true, 'Specified Trade Id doesnot exist', 500, null)
                                        res.send(apiResponse)
                                    }else{
                                        const existingTPortfolioDetails=result;
                                        let existingPortfolioShares;
                                        let existingPortfolioAvBuyPrice;
                                        if(newTradeDetails.newTradeType === "buy" && existingTransaction[0].tradeType === "buy"){
                                            existingPortfolioShares = parseInt(existingTPortfolioDetails[0].noOfShares) - parseInt(existingTransaction[0].noOfShares);
                                            existingPortfolioAvBuyPrice = ((existingTPortfolioDetails[0].noOfShares*existingTPortfolioDetails[0].averageBuyPrice) - (existingTransaction[0].noOfShares*existingTransaction[0].tradePrice))/(existingPortfolioShares); 
                                        }
                                        else if(newTradeDetails.newTradeType === "buy" && existingTransaction[0].tradeType === "sell"){
                                            existingPortfolioShares = parseInt(existingTPortfolioDetails[0].noOfShares) + parseInt(existingTransaction[0].noOfShares);
                                            //av buy remains same!
                                            existingPortfolioAvBuyPrice = existingTPortfolioDetails[0].averageBuyPrice;
                                        }
                                        //add to transaction and update portfolio
                                        stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true", existingTransaction[0].tradeId ).then((resolve)=>{
                                            stockPortfolio.addToPortfolio(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, resolve.transactionId).then((innerResolve)=>{
                                                stockPortfolio.updatePortfolioBySymbol(existingTPortfolioDetails[0].tickerSymbol, existingPortfolioAvBuyPrice, existingPortfolioShares, resolve.transactionId).then((innerResolve1)=>{
                                                    logger.info(innerResolve1, 'updateStockTradeController diff stock and buy, buy call: updateTrade', 10);
                                                    const resp = { 
                                                        tradeId : resolve.tradeId 
                                                    };
                                                    const apiResponse = response.generate(false, 'Trade updated successfully', 200, resp)
                                                    res.send(apiResponse)
                                                    }).catch((err)=>{
                                                        logger.error(err.message, 'updateStockTradeController diff stock and buy, buy call: updateTrade', 10);
                                                        const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                        res.send(apiResponse)
                                                    })
                                            }).catch((err)=>{
                                                logger.error(err.message, 'updateStockTradeController diff stock and buy, buy call: updateTrade', 10);
                                                const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                res.send(apiResponse)
                                            })
                                        }).catch((err)=>{
                                            logger.error(err.message, 'updateStockTradeController diff stock and buy, buy call: updateTrade', 10);
                                            const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                            res.send(apiResponse)
                                        })
                                    } 
                                })
                            }
                        }else{
                            let apiResponse = response.generate(false, 'Trade Details Found', 200, result)
                            const portfolioDetails = result;
                            //Update logic starts
                            //if existing transaction's stock is same as updating transaction's stock
                            if( existingTransaction[0].tickerSymbol === newTradeDetails.newSymbol ){
                                if(newTradeDetails.newTradeType === "buy"){
                                    let newPortfolioShares, newPortfolioAvBuyPrice;
                                    if(existingTransaction[0].tradeType === "buy"){
                                         //calculate shares and averageBuyPrice
                                        newPortfolioShares = parseInt(portfolioDetails[0].noOfShares) - parseInt(existingTransaction[0].noOfShares) + parseInt(newTradeDetails.newShares) ;
                                        newPortfolioAvBuyPrice = ((portfolioDetails[0].noOfShares*portfolioDetails[0].averageBuyPrice) - (existingTransaction[0].noOfShares*existingTransaction[0].tradePrice) + (newTradeDetails.newShares* newTradeDetails.newTradePrice))/(newPortfolioShares)
                                    }
                                    else if(existingTransaction[0].tradeType === "sell"){
                                         //calculate shares and averageBuyPrice
                                        newPortfolioShares = parseInt(portfolioDetails[0].noOfShares) + parseInt(existingTransaction[0].noOfShares) + parseInt(newTradeDetails.newShares) ;
                                        newPortfolioAvBuyPrice = ((portfolioDetails[0].noOfShares*portfolioDetails[0].averageBuyPrice) + (existingTransaction[0].noOfShares*portfolioDetails[0].averageBuyPrice) + (newTradeDetails.newShares* newTradeDetails.newTradePrice))/(newPortfolioShares)
                                    }
                                    //update old transaction with recent as false
                                    stockTransaction.ageTransactionById(existingTransaction[0].transactionId);
                                    //add to transaction collection and update portfolio collection
                                    stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true", existingTransaction[0].tradeId ).then((resolve)=>{
                                        stockPortfolio.updatePortfolioBySymbol(newTradeDetails.newSymbol, newPortfolioAvBuyPrice, newPortfolioShares, resolve.transactionId).then((innerResolve)=>{
                                        logger.info(innerResolve, 'updateStockTradeController same stock and buy call: updateTrade', 10);
                                        const resp = { 
                                            tradeId : resolve.tradeId 
                                        };
                                        const apiResponse = response.generate(false, 'Trade updated successfully', 200, resp)
                                        res.send(apiResponse)
                                        }).catch((err)=>{
                                            logger.error(err.message, 'updateStockTradeController same stock and buy call: updateTrade', 10);
                                            const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                            res.send(apiResponse)
                                        })
                                    }).catch((err)=>{
                                        logger.error(err.message, 'updateStockTradeController same stock and buy call: updateTrade', 10);
                                        const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                        res.send(apiResponse)
                                    })
                                }
                                else if(newTradeDetails.newTradeType === "sell"){
                                    if(existingTransaction[0].tradeType === "buy"){
                                         //calculate shares and averageBuyPrice
                                         newPortfolioShares = parseInt(portfolioDetails[0].noOfShares) - parseInt(existingTransaction[0].noOfShares) - parseInt(newTradeDetails.newShares) ;
                                         if(newPortfolioShares>=0){
                                            if(newPortfolioShares === 0){
                                                //delete the stock from portfolio
                                                stockPortfolio.deleteTickerSymbolPortfolio(portfolioDetails[0].tickerSymbol);
                                                //update old transaction with recent as false
                                                stockTransaction.ageTransactionById(existingTransaction[0].transactionId);
                                                //add to transaction
                                                stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true", existingTransaction[0].tradeId ).then((resolve)=>{
                                                const resp = { 
                                                    tradeId : resolve.tradeId 
                                                };
                                                const apiResponse = response.generate(false, 'Trade updated successfully', 200, resp)
                                                res.send(apiResponse)
                                                }).catch((err)=>{
                                                    logger.error(err.message, 'updateStockTradeController same stock and sell,buy call: updateTrade', 10);
                                                    const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                    res.send(apiResponse)
                                                })
                                            }
                                            else{
                                                //Calculate average price
                                                const tempVar = parseInt(portfolioDetails[0].noOfShares) - parseInt(existingTransaction[0].noOfShares)
                                                newPortfolioAvBuyPrice = ((portfolioDetails[0].noOfShares*portfolioDetails[0].averageBuyPrice) - (existingTransaction[0].noOfShares*existingTransaction[0].tradePrice))/(tempVar);
                                                //update old transaction with recent as false
                                                stockTransaction.ageTransactionById(existingTransaction[0].transactionId);
                                                //add to transaction collection and update portfolio collection
                                                stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true", existingTransaction[0].tradeId ).then((resolve)=>{
                                                    stockPortfolio.updatePortfolioBySymbol(newTradeDetails.newSymbol, newPortfolioAvBuyPrice, newPortfolioShares, resolve.transactionId).then((innerResolve)=>{
                                                    logger.info(innerResolve, 'updateStockTradeController same stock and sell, buy call: updateTrade', 10);
                                                    const resp = { 
                                                        tradeId : resolve.tradeId 
                                                    };
                                                    const apiResponse = response.generate(false, 'Trade updated successfully', 200, resp)
                                                    res.send(apiResponse)
                                                    }).catch((err)=>{
                                                        logger.error(err.message, 'updateStockTradeController same stock and sell, buy call: updateTrade', 10);
                                                        const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                        res.send(apiResponse)
                                                    })
                                                }).catch((err)=>{
                                                    logger.error(err.message, 'updateStockTradeController same stock and sell, buy call: updateTrade', 10);
                                                    const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                    res.send(apiResponse)
                                                })
                                            }
                                         }
                                         else{
                                            logger.error("err.message", 'updateStockTradeController same stock and sell, buy call: updateTrade', 10);
                                             //error! -> you dont have enough stocks to sell!
                                            let apiResponse = response.generate(true, 'You do not have enough stocks to sell', 500, null)
                                            res.send(apiResponse)
                                         }  
                                    }
                                    else if(existingTransaction[0].tradeType === "sell"){
                                        if(existingTransaction[0].noOfShares === newTradeDetails.newShares){
                                            //no change in portfolio!
                                            //update old transaction with recent as false
                                            stockTransaction.ageTransactionById(existingTransaction[0].transactionId);
                                            //add to transaction
                                            stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true", existingTransaction[0].tradeId ).then((resolve)=>{
                                            const resp = { 
                                                tradeId : resolve.tradeId 
                                            };
                                            const apiResponse = response.generate(false, 'Trade updated successfully', 200, resp)
                                            res.send(apiResponse)
                                            }).catch((err)=>{
                                                logger.error(err.message, 'updateStockTradeController same stock and sell,sell call: updateTrade', 10);
                                                const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                res.send(apiResponse)
                                            })
                                        }
                                        else{
                                            //calculate shares
                                            const newPortfolioShares = parseInt(portfolioDetails[0].noOfShares) + parseInt(existingTransaction[0].noOfShares) - parseInt(newTradeDetails.newShares) ;
                                            if(newPortfolioShares>=0){
                                                if(newPortfolioShares === 0){
                                                    //delete the stock from portfolio
                                                    stockPortfolio.deleteTickerSymbolPortfolio(portfolioDetails[0].tickerSymbol);
                                                    //update old transaction with recent as false
                                                    stockTransaction.ageTransactionById(existingTransaction[0].transactionId);
                                                    //add to transaction
                                                    stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true", existingTransaction[0].tradeId ).then((resolve)=>{
                                                    const resp = { 
                                                        tradeId : resolve.tradeId 
                                                    };
                                                    const apiResponse = response.generate(false, 'Trade updated successfully', 200, resp)
                                                    res.send(apiResponse)
                                                    }).catch((err)=>{
                                                        logger.error(err.message, 'updateStockTradeController same stock and sell,sell call: updateTrade', 10);
                                                        const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                        res.send(apiResponse)
                                                    })
                                                }
                                                else{
                                                    //Average price remains same!
                                                    //update old transaction with recent as false
                                                    stockTransaction.ageTransactionById(existingTransaction[0].transactionId);
                                                    //add to transaction collection and update portfolio collection
                                                    stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true", existingTransaction[0].tradeId ).then((resolve)=>{
                                                        stockPortfolio.updatePortfolioBySymbol(newTradeDetails.newSymbol, portfolioDetails[0].averageBuyPrice, newPortfolioShares, resolve.transactionId).then((innerResolve)=>{
                                                        logger.info(innerResolve, 'updateStockTradeController same stock and sell, buy call: updateTrade', 10);
                                                        const resp = { 
                                                            tradeId : resolve.tradeId 
                                                        };
                                                        const apiResponse = response.generate(false, 'Trade updated successfully', 200, resp)
                                                        res.send(apiResponse)
                                                        }).catch((err)=>{
                                                            logger.error(err.message, 'updateStockTradeController same stock and sell, sell call: updateTrade', 10);
                                                            const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                            res.send(apiResponse)
                                                        })
                                                    }).catch((err)=>{
                                                        logger.error(err.message, 'updateStockTradeController same stock and sell, sell call: updateTrade', 10);
                                                        const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                        res.send(apiResponse)
                                                    })
                                                }
                                            }
                                            else{
                                                logger.error("err.message", 'updateStockTradeController same stock and sell, sell call: updateTrade', 10);
                                                //error! -> you dont have enough stocks to sell!
                                                let apiResponse = response.generate(true, 'You do not have enough stocks to sell', 500, null)
                                                res.send(apiResponse)
                                            }  
                                        }
                                    }
                                }   
                                
                            }
                            //check if existing transaction symbol and updating transaction symbol is different
                            else if(existingTransaction[0].tickerSymbol !== newTradeDetails.newSymbol ){
                                stockPortfolioModel.find({ tickerSymbol : existingTransaction[0].tickerSymbol}) //dname
                                .lean()
                                .exec((err,result)=>{
                                    if(err){
                                        logger.error(err.message, 'updateStockTradeController: findSymbol in portfolio', 10)
                                        let apiResponse = response.generate(true, 'Specified Trade Id doesnot exist', 500, null)
                                        res.send(apiResponse)   
                                    }else if (check.isEmpty(result)){
                                        //Previously transacted stock details not found in prtfolio collection
                                        //add to transaction collection and portfolio collection
                                        stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true").then((resolve)=>{
                                            stockPortfolio.addToPortfolio(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, resolve.transactionId).then((innerResolve)=>{
                                            const resp = { 
                                                tradeId : resolve.tradeId 
                                            };
                                            const apiResponse = response.generate(false, 'Trade executed successfully', 200, resp)
                                            res.send(apiResponse)
                                            }).catch((err)=>{
                                                logger.error(err.message, 'addStockTradeController: saveTrade', 10);
                                                const apiResponse = response.generate(true, 'Failed To execute trade', 500, null);
                                                res.send(apiResponse)
                                            })
                                        }).catch((err)=>{
                                            logger.error(err.message, 'addStockTradeController: saveTrade', 10);
                                            const apiResponse = response.generate(true, 'Failed To execute trade', 500, null);
                                            res.send(apiResponse)
                                        })
                                    }else{
                                        //Previously transacted stock details found in prtfolio collection
                                        const existingTPortfolioDetails=result;
                                        //update old transaction with recent as false
                                        stockTransaction.ageTransactionById(existingTransaction[0].transactionId);
                                        //calculate shares of updating stock
                                        const newPortfolioShares = parseInt(portfolioDetails[0].noOfShares) + parseInt(newTradeDetails.newShares);

                                        if(newTradeDetails.newTradeType === "buy"){
                                            if(existingTransaction[0].tradeType === "buy"){
                                                //calculate shares and averageBuyPrice
                                                const existingPortfolioShares = parseInt(existingTPortfolioDetails[0].noOfShares) - parseInt(existingTransaction[0].noOfShares);
                                                const existingPortfolioAvBuyPrice = ((existingTPortfolioDetails[0].noOfShares*existingTPortfolioDetails[0].averageBuyPrice) - (existingTransaction[0].noOfShares*existingTransaction[0].tradePrice))/(existingPortfolioShares);
                                                //update old transaction with recent as false
                                                stockTransaction.ageTransactionById(existingTransaction[0].transactionId);
                                                
                                                const newPortfolioAvBuyPrice = ((portfolioDetails[0].noOfShares*portfolioDetails[0].averageBuyPrice) + (newTradeDetails.newShares*newTradeDetails.newTradePrice))/(newPortfolioShares);
                                                //add to transaction and update portfolio for both shares
                                                stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true", existingTransaction[0].tradeId ).then((resolve)=>{
                                                    stockPortfolio.updatePortfolioBySymbol(newTradeDetails.newSymbol, newPortfolioAvBuyPrice, newPortfolioShares, resolve.transactionId).then((innerResolve)=>{
                                                        stockPortfolio.updatePortfolioBySymbol(existingTPortfolioDetails[0].tickerSymbol, existingPortfolioAvBuyPrice, existingPortfolioShares, resolve.transactionId).then((innerResolve1)=>{
                                                            logger.info(innerResolve1, 'updateStockTradeController diff stock and buy, buy call: updateTrade', 10);
                                                            const resp = { 
                                                                tradeId : resolve.tradeId 
                                                            };
                                                            const apiResponse = response.generate(false, 'Trade updated successfully', 200, resp)
                                                            res.send(apiResponse)
                                                            }).catch((err)=>{
                                                                logger.error(err.message, 'updateStockTradeController diff stock and buy, buy call: updateTrade', 10);
                                                                const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                                res.send(apiResponse)
                                                            })
                                                    }).catch((err)=>{
                                                        logger.error(err.message, 'updateStockTradeController diff stock and buy, buy call: updateTrade', 10);
                                                        const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                        res.send(apiResponse)
                                                    })
                                                }).catch((err)=>{
                                                    logger.error(err.message, 'updateStockTradeController diff stock and buy, buy call: updateTrade', 10);
                                                    const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                    res.send(apiResponse)
                                                })
                                            }
                                            else if(existingTransaction[0].tradeType === "sell"){
                                                //calculate shares and averageBuyPrice remains same!
                                                const existingPortfolioShares = parseInt(existingTPortfolioDetails[0].noOfShares) + parseInt(existingTransaction[0].noOfShares);
                                                
                                                const newPortfolioAvBuyPrice = ((portfolioDetails[0].noOfShares*portfolioDetails[0].averageBuyPrice) + (newTradeDetails.newShares*newTradeDetails.newTradePrice))/(newPortfolioShares);
                                                //add to transaction and update portfolio for both shares
                                                stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true", existingTransaction[0].tradeId ).then((resolve)=>{
                                                    stockPortfolio.updatePortfolioBySymbol(newTradeDetails.newSymbol, newPortfolioAvBuyPrice, newPortfolioShares, resolve.transactionId).then((innerResolve)=>{
                                                        stockPortfolio.updatePortfolioBySymbol(existingTPortfolioDetails[0].tickerSymbol, existingPortfolioAvBuyPrice, existingPortfolioShares, resolve.transactionId).then((innerResolve1)=>{
                                                            logger.info(innerResolve1, 'updateStockTradeController diff stock and buy, buy call: updateTrade', 10);
                                                            const resp = { 
                                                                tradeId : resolve.tradeId 
                                                            };
                                                            const apiResponse = response.generate(false, 'Trade updated successfully', 200, resp)
                                                            res.send(apiResponse)
                                                            }).catch((err)=>{
                                                                logger.error(err.message, 'updateStockTradeController diff stock and buy, buy call: updateTrade', 10);
                                                                const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                                res.send(apiResponse)
                                                            })
                                                    }).catch((err)=>{
                                                        logger.error(err.message, 'updateStockTradeController diff stock and buy, buy call: updateTrade', 10);
                                                        const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                        res.send(apiResponse)
                                                    })
                                                }).catch((err)=>{
                                                    logger.error(err.message, 'updateStockTradeController diff stock and buy, buy call: updateTrade', 10);
                                                    const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                    res.send(apiResponse)
                                                })
                                                
                                            }
                                        }
                                        else if(newTradeDetails.newTradeType === "sell"){
                                            if(existingTransaction[0].tradeType === "buy"){
                                                //calculate shares of updating stock
                                                const newPortfolioShares = parseInt(portfolioDetails[0].noOfShares) - parseInt(newTradeDetails.newShares);
                                                if(newPortfolioShares >= 0){
                                                    //update old transaction with recent as false
                                                    stockTransaction.ageTransactionById(existingTransaction[0].transactionId);
                                                    const existingPortfolioShares = parseInt(existingTPortfolioDetails[0].noOfShares) - parseInt(existingTransaction[0].noOfShares);
                                                    if(existingPortfolioShares === 0){
                                                        //delete the stock from portfolio
                                                        stockPortfolio.deleteTickerSymbolPortfolio(existingTPortfolioDetails[0].tickerSymbol);
                                                    }
                                                    if(newPortfolioShares === 0){
                                                        //calculate av buy price for existing stock
                                                        const existingPortfolioAvBuyPrice = ((existingTPortfolioDetails[0].noOfShares*existingTPortfolioDetails[0].averageBuyPrice) - (existingTransaction[0].noOfShares*existingTransaction[0].tradePrice))/(existingPortfolioShares);

                                                        //delete stock from portfolio
                                                        stockPortfolio.deleteTickerSymbolPortfolio(portfolioDetails[0].tickerSymbol);
                                                        
                                                        //add to transaction collection and update portfolio collection
                                                        stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true", existingTransaction[0].tradeId ).then((resolve)=>{
                                                            stockPortfolio.updatePortfolioBySymbol(existingTPortfolioDetails[0].tickerSymbol, existingPortfolioAvBuyPrice, existingPortfolioShares, resolve.transactionId).then((innerResolve)=>{
                                                            logger.info(innerResolve, 'updateStockTradeController diff stock and sell, buy call: updateTrade', 10);
                                                            const resp = { 
                                                                tradeId : resolve.tradeId 
                                                            };
                                                            const apiResponse = response.generate(false, 'Trade updated successfully', 200, resp)
                                                            res.send(apiResponse)
                                                            }).catch((err)=>{
                                                                logger.error(err.message, 'updateStockTradeController diff stock and sell, buy call: updateTrade', 10);
                                                                const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                                res.send(apiResponse)
                                                            })
                                                        }).catch((err)=>{
                                                            logger.error(err.message, 'updateStockTradeController diff stock and sell, buy call: updateTrade', 10);
                                                            const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                            res.send(apiResponse)
                                                        })
                                                    }else{
                                                        //average buy price remains same!
                                                        //calculate av buy price for existing stock
                                                        const existingPortfolioAvBuyPrice = ((existingTPortfolioDetails[0].noOfShares*existingTPortfolioDetails[0].averageBuyPrice) - (existingTransaction[0].noOfShares*existingTransaction[0].tradePrice))/(existingPortfolioShares);
                                                        //add to transaction and update portfolio for both shares
                                                        stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true", existingTransaction[0].tradeId ).then((resolve)=>{
                                                            stockPortfolio.updatePortfolioBySymbol(newTradeDetails.newSymbol, portfolioDetails[0].averageBuyPrice, newPortfolioShares, resolve.transactionId).then((innerResolve)=>{
                                                                stockPortfolio.updatePortfolioBySymbol(existingTPortfolioDetails[0].tickerSymbol, existingPortfolioAvBuyPrice, existingPortfolioShares, resolve.transactionId).then((innerResolve1)=>{
                                                                    logger.info(innerResolve1, 'updateStockTradeController diff stock and sell, buy call: updateTrade', 10);
                                                                    const resp = { 
                                                                        tradeId : resolve.tradeId 
                                                                    };
                                                                    const apiResponse = response.generate(false, 'Trade updated successfully', 200, resp)
                                                                    res.send(apiResponse)
                                                                    }).catch((err)=>{
                                                                        logger.error(err.message, 'updateStockTradeController diff stock and sell, buy call: updateTrade', 10);
                                                                        const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                                        res.send(apiResponse)
                                                                    })
                                                            }).catch((err)=>{
                                                                logger.error(err.message, 'updateStockTradeController diff stock and sell, buy call: updateTrade', 10);
                                                                const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                                res.send(apiResponse)
                                                            })
                                                        }).catch((err)=>{
                                                            logger.error(err.message, 'updateStockTradeController diff stock and sell, buy call: updateTrade', 10);
                                                            const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                            res.send(apiResponse)
                                                        })
                                                        
                                                    }
                                                }
                                                else{
                                                    logger.error("err.message", 'updateStockTradeController diff stock and sell, buy call: updateTrade', 10);
                                                    //error! -> you dont have enough stocks to sell!
                                                    let apiResponse = response.generate(true, 'You do not have enough stocks to sell', 500, null)
                                                    res.send(apiResponse)
                                                }
                                            }
                                            else if(existingTransaction[0].tradeType === "sell"){
                                                 //calculate shares of updating stock
                                                 const newPortfolioShares = parseInt(portfolioDetails[0].noOfShares) - parseInt(newTradeDetails.newShares);
                                                 if(newPortfolioShares >= 0){
                                                     //update old transaction with recent as false
                                                     stockTransaction.ageTransactionById(existingTransaction[0].transactionId);
                                                     const existingPortfolioShares = parseInt(existingTPortfolioDetails[0].noOfShares) + parseInt(existingTransaction[0].noOfShares);

                                                     if(newPortfolioShares === 0){
                                                         //Av buy price remains same

                                                         //delete stock from portfolio
                                                         stockPortfolio.deleteTickerSymbolPortfolio(portfolioDetails[0].tickerSymbol);
                                                         
                                                         //add to transaction collection and update portfolio collection
                                                         stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true", existingTransaction[0].tradeId ).then((resolve)=>{
                                                             stockPortfolio.updatePortfolioBySymbol(existingTPortfolioDetails[0].tickerSymbol, existingTPortfolioDetails[0].averageBuyPrice, existingPortfolioShares, resolve.transactionId).then((innerResolve)=>{
                                                             logger.info(innerResolve, 'updateStockTradeController diff stock and sell, sell call: updateTrade', 10);
                                                             const resp = { 
                                                                 tradeId : resolve.tradeId 
                                                             };
                                                             const apiResponse = response.generate(false, 'Trade updated successfully', 200, resp)
                                                             res.send(apiResponse)
                                                             }).catch((err)=>{
                                                                 logger.error(err.message, 'updateStockTradeController diff stock and sell, sell call: updateTrade', 10);
                                                                 const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                                 res.send(apiResponse)
                                                             })
                                                         }).catch((err)=>{
                                                             logger.error(err.message, 'updateStockTradeController diff stock and sell, sell call: updateTrade', 10);
                                                             const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                             res.send(apiResponse)
                                                         })
                                                     }else{
                                                         //average buy price remains same!
                                                         //average buy price remains same for existing stock!

                                                         //add to transaction and update portfolio for both shares
                                                         stockTransaction.addToTransaction(newTradeDetails.newSymbol, newTradeDetails.newTradePrice, newTradeDetails.newShares, newTradeDetails.newTradeType, recent="true", existingTransaction[0].tradeId ).then((resolve)=>{
                                                             stockPortfolio.updatePortfolioBySymbol(newTradeDetails.newSymbol, portfolioDetails[0].averageBuyPrice, newPortfolioShares, resolve.transactionId).then((innerResolve)=>{
                                                                 stockPortfolio.updatePortfolioBySymbol(existingTPortfolioDetails[0].tickerSymbol, existingTPortfolioDetails[0].averageBuyPrice, existingPortfolioShares, resolve.transactionId).then((innerResolve1)=>{
                                                                     logger.info(innerResolve1, 'updateStockTradeController diff stock and sell, sell call: updateTrade', 10);
                                                                     const resp = { 
                                                                         tradeId : resolve.tradeId 
                                                                     };
                                                                     const apiResponse = response.generate(false, 'Trade updated successfully', 200, resp)
                                                                     res.send(apiResponse)
                                                                     }).catch((err)=>{
                                                                         logger.error(err.message, 'updateStockTradeController diff stock and sell, sell call: updateTrade', 10);
                                                                         const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                                         res.send(apiResponse)
                                                                     })
                                                             }).catch((err)=>{
                                                                 logger.error(err.message, 'updateStockTradeController diff stock and sell, sell call: updateTrade', 10);
                                                                 const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                                 res.send(apiResponse)
                                                             })
                                                         }).catch((err)=>{
                                                             logger.error(err.message, 'updateStockTradeController diff stock and sell, sell call: updateTrade', 10);
                                                             const apiResponse = response.generate(true, 'Failed To update trade', 500, null);
                                                             res.send(apiResponse)
                                                         })
                                                         
                                                     }
                                                 }
                                                 else{
                                                     logger.error("err.message", 'updateStockTradeController diff stock and sell, sell call: updateTrade', 10);
                                                     //error! -> you dont have enough stocks to sell!
                                                     let apiResponse = response.generate(true, 'You do not have enough stocks to sell', 500, null)
                                                     res.send(apiResponse)
                                                 }
                                            }
                                        } 
                                    }
                                })
                            }
                        }
                    })
                }
            })
        })
    } 
    validateTrade(req, res)
    .then(updateTrade)
    .then((resolve)=>{
        let apiResponse = response.generate(false, 'Trade executed successfully', 200, resolve)
        res.send(apiResponse)
    })
    .catch((err)=>{
        logger.error(err.message, 'stockTradeController: AddTrade', 10);
        let apiResponse = response.generate(true, 'Failed To Update Trade', 500, null);
        res.send(err)
    })

}//end of update trade

//---------------------------------END OF UPDATE TRADE -------------------------------------//

module.exports = {
    editTransaction,
}