const mongoose = require('mongoose'),
Schema = mongoose.Schema;
var Float = require('mongoose-float').loadType(mongoose);


let stockTransactionSchema = new Schema({

    transactionId: {
        type: String,
        required: true,
    },

    tradeId: {
        type: String,
        required: true,
    },

    tickerSymbol :{
        type: String,
        required: true,
    },

    tradePrice: {
        type: Float,
        required: true,
    },

    noOfShares: {
        type: Number,
        required: true,
    },
 
    tradeType: {
        type: String,
        required: true,
    },

    recent: {
        type: Boolean,
        default: true,
    },
    transactionTime: { 
        type : Date, 
        default: Date.now 
    },

})


mongoose.model('stockTransactionModel', stockTransactionSchema);