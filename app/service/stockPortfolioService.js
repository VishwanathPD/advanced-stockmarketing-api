const mongoose = require('mongoose');
const shortid = require('shortid');

const logger = require('../libs/loggerLib');
const response = require('../libs/responseLib')
const check = require('../libs/checkLib');

const stockPortfolioM =  require('../models/stockPortfolio');
const stockTransactionM = require('../models/stockTransaction');
const stockTransactionModel = mongoose.model('stockTransactionModel');
const stockPortfolioModel = mongoose.model('stockPortfolioModel');


function addToPortfolio(tickerSymbol, avTradePrice, shares, transactionId){
    return new Promise( (resolve, reject)=>{
        const newTrade = stockPortfolioModel({
            portfolioId: shortid.generate(),
            tickerSymbol: tickerSymbol,
            averageBuyPrice: avTradePrice,
            noOfShares: shares,
            transactionId: transactionId,
        })
        newTrade.save((err, newTrade)=>{
            if(err){
                logger.error(err.message, 'stockPortfolio: addToPortfolio', 10);
                let apiResponse = response.generate(true, 'Failed To execute trade', 500, null);
                reject(apiResponse);
            }else{
                logger.info(newTrade, 'stockPortfolio: addToPortfolio ', 10)
                let apiResponse = response.generate(false, 'Added to portfolio successfully', 200, newTrade)
                resolve(apiResponse);
            }
        }) 
    })
}

function getPortfolioBySymbol(tickerSymbol){
    logger.info("err.message", 'stockPortfolio: getPortfolioBySymbol', 10)

    return {
        portfolioSymbol: tickerSymbol,
        portfolioAvPrice: "averageBuyPrice",
        portfolioShares: "noOfShares",
        portfolioTransactionId: "transactionId"
    }
}

function updatePortfolioBySymbol(tickerSymbol, avTradePrice, shares, transactionId){
    return new Promise( (resolve, reject)=>{
        const updateOptions = {
            tickerSymbol: tickerSymbol,
            averageBuyPrice: avTradePrice,
            noOfShares: shares,
            transactionId: transactionId,
        }
        const filter= { tickerSymbol : tickerSymbol };
        stockPortfolioModel.findOneAndUpdate(filter, { $set: updateOptions })
        .exec((err,result)=>{
            if(err){
                logger.error(err.message, 'stockPortfolio: updatePortfolioBySymbol ', 10)
                let apiResponse = response.generate(true, 'Failed to update Portfolio', 500, null)
                reject(apiResponse);
            }else if (check.isEmpty(result)){
                logger.error("Empty result", 'stockPortfolio: updatePortfolioBySymbol ', 10)
                let apiResponse = response.generate(true, 'Failed to update Portfolio', 404, null)
                reject(apiResponse);
            }else{
                logger.info(result, 'stockPortfolio: updatePortfolioBySymbol ', 10)
                let apiResponse = response.generate(false, 'Updated portfolio successfully', 200, result)
                resolve(apiResponse);
            }
        })
    })
}


function deleteTickerSymbolPortfolio(tickerSymbol){
    return new Promise( (resolve, reject)=>{
        const filter= { tickerSymbol : tickerSymbol };
        logger.error(filter, 'stockPortfolio: deleteTickerSymbolPortfolio', 10)
        stockPortfolioModel.findOneAndRemove(filter)
            .exec((err,result)=>{
                if(err){
                    logger.error(err.message, 'stockPortfolio: deleteTickerSymbolPortfolio', 10)
                    let apiResponse = response.generate(true, 'Failed to delete stock', 500, null)
                    reject(apiResponse);
                }else if (check.isEmpty(result)){
                    logger.error("empty", 'stockPortfolio: deleteTickerSymbolPortfolio', 10)
                    let apiResponse = response.generate(true, 'No stock Found', 404, null)
                    resolve(apiResponse);
                }else{
                    logger.info(result, 'stockPortfolio: deleteTickerSymbolPortfolio ', 10)
                    let apiResponse = response.generate(false, 'Stock Deleted successfully', 200, result)
                    resolve(apiResponse);
                }
            })
    })
    
}

module.exports = {
    addToPortfolio,
    getPortfolioBySymbol,
    deleteTickerSymbolPortfolio,
    updatePortfolioBySymbol,
}




