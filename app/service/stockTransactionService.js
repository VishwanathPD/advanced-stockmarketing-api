const mongoose = require('mongoose');
const shortid = require('shortid');

const logger = require('../libs/loggerLib');
const response = require('../libs/responseLib')
const check = require('../libs/checkLib');

const stockPortfolioM =  require('../models/stockPortfolio');
const stockTransactionM = require('../models/stockTransaction');
const stockTransactionModel = mongoose.model('stockTransactionModel');
const stockPortfolioModel = mongoose.model('stockPortfolioModel');

const addToTransaction= (newSymbol, newTradePrice,newShares, newTradeType, recent, tradeId) => {
    return new Promise( (resolve, reject)=>{
        let newTrade = stockTransactionModel({
            transactionId: shortid.generate(),
            tradeId: tradeId || shortid.generate(),
            tickerSymbol: newSymbol,
            tradePrice: newTradePrice,
            noOfShares: newShares,
            tradeType: newTradeType,
            recent: recent,
        })
        newTrade.save((err, newTrade)=>{
            if(err){
                logger.error(err.message, 'stockTransaction: addToTransaction', 10);
                let apiResponse = response.generate(true, 'Failed To execute trade', 500, null);
                reject(apiResponse);
            }else{
                let returnJson = {
                    transactionId: newTrade.transactionId,
                    tradeId: newTrade.tradeId
                };
                // return newTrade;
                resolve(returnJson);
            }
        }) 
    })
}


const ageTransactionById = (transactionId) => {
    return new Promise( (resolve, reject)=>{
        const updateOptions = {
            recent: "false"
        }
        const filter= { transactionId: transactionId, };
        stockTransactionModel.findOneAndUpdate(filter, { $set: updateOptions })
        .exec((err,result)=>{
            if(err){
                logger.error(err.message, 'stockTransaction: ageTransactionById ', 10)
                let apiResponse = response.generate(true, 'Failed to age Transaction', 500, null)
                reject(apiResponse);
            }else if (check.isEmpty(result)){
                logger.error("Empty result", 'stockTransaction: ageTransactionById ', 10)
                let apiResponse = response.generate(true, 'Transaction not found', 404, null)
                reject(apiResponse);
            }else{
                logger.info(result, 'stockTransaction: ageTransactionById ', 10)
                let apiResponse = response.generate(false, 'Aged Transaction successfully', 200, result)
                resolve(apiResponse);
            }
        })
    })
}


module.exports = {
    addToTransaction,
    ageTransactionById
}




