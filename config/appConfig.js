let appConfig = {};

//app config
//mngodb is hosted on Mangodb Atlas[AWS EC2]

appConfig.port = process.env.PORT || 3000;
appConfig.allowedCorsOrigin = "*";
appConfig.env = "dev";
appConfig.db = {
    uri: 'mongodb+srv://vishwa:vishwa@cluster0.kvy5d.mongodb.net/smallcase?retryWrites=true&w=majority'
  }
appConfig.apiVersion = '/api/v1';


module.exports = {
    port: appConfig.port,
    allowedCorsOrigin: appConfig.allowedCorsOrigin,
    environment: appConfig.env,
    db :appConfig.db,
    apiVersion : appConfig.apiVersion
};